package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;
/**
 * Servlet implementation class UserSignupServlet
 */
@WebServlet("/UserSignupServlet")
public class UserSignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSignupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserBeans user=(UserBeans)session.getAttribute("getUser");
		if(user==null) {
			response.sendRedirect("LoginServlet");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		UserBeans user=(UserBeans)session.getAttribute("getUser");
		if(user==null) {
			response.sendRedirect("LoginServlet");
		}

		String loginId = request.getParameter("loginId");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String userName = request.getParameter("userName");
		String userBirthDate = request.getParameter("birthDate");



		if (!(password1.equals(password2)) || password1.equals("") || password2.equals("") || userName.equals("")
				|| userBirthDate == null) {
			request.setAttribute("alert", "入力された内容が正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserDao dao=new UserDao();
		UserBeans loginCheck=dao.getUserInfobyLoginId(loginId);

		if(!(loginCheck==null)) {
			request.setAttribute("alert", "そのログインIDは既に使用されています。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDao md5=new UserDao();

		UserDao dao2 = new UserDao();
		dao2.signup(loginId, md5.encode(password1), userName, userBirthDate);
		response.sendRedirect("UserListServlet");

	}

}

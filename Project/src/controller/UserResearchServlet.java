package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class ReserchServlet
 */
@WebServlet("/UserResearchServlet")
public class UserResearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserResearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		UserBeans user=(UserBeans)session.getAttribute("getUser");
		if(user==null) {
			response.sendRedirect("LoginServlet");
		}
			//userduoのreseaechメソッド（未作成）を起動
			String loginId=request.getParameter("loginId");
			String userName=request.getParameter("userName");
			String birthDate1=request.getParameter("birthDate1");
			String birthDate2=request.getParameter("birthDate2");

			UserDao dao=new UserDao();
			List<UserBeans> userList = dao.research(loginId,userName,birthDate1,birthDate2);
			request.setAttribute("userList",userList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
			dispatcher.forward(request, response);



	}

}

package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session=request.getSession();
		UserBeans user=(UserBeans)session.getAttribute("getUser");
		if(user==null) {
			response.sendRedirect("LoginServlet");
		}

		String id=request.getParameter("id");
		UserDao dao=new UserDao();
		UserBeans userList=dao.getUserInfo(id);
		request.setAttribute("userList",userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		UserBeans user=(UserBeans)session.getAttribute("getUser");
		if(user==null) {
			response.sendRedirect("LoginServlet");
		}

		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String userName = request.getParameter("userName");
		String userBirthDate = request.getParameter("birthDate");
		String id =request.getParameter("userId");
		//↓未完成。メンターに聞く

		UserDao dao=new UserDao();
		if (!(password1.equals(password2))  || userName.equals("")
				|| userBirthDate.equals("")) {
			request.setAttribute("alert", "入力された内容が正しくありません。");
			UserBeans userList=dao.getUserInfo(id);
			System.out.println(id);
			request.setAttribute("userList",userList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}


		if(password1.equals("") && password2.equals("")) {
			dao.update(userName, userBirthDate, id);
			response.sendRedirect("UserListServlet");
			return;
		}

		UserDao md5=new UserDao();
		dao.update(md5.encode(password1), userName, userBirthDate, id);
		response.sendRedirect("UserListServlet");

	}

}

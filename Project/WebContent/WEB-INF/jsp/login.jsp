<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body style="margin-right: auto; margin-left: auto; font-family: serif; border: solid 2px; width: 700px; height: 400px">

<c:if test="${alert!=null}">
<h6>${alert}</h6>
</c:if>

	<br>
	<h3 style="text-align: center">ログイン画面</h3>

	<br>
	<br>
	<form action="LoginServlet" method="post">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-2">
				<h6>ログインID</h6>
			</div>
			<input type="text" name="loginId" style="width: 200px; height: 30px"
				class="form-control">

		</div>
		<br>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-2">
				<h6>パスワード</h6>
			</div>
			<input type="password" name="password"
				style="width: 200px; height: 30px" class="form-control">
		</div>
		<br> <br>
		<div class="row">
			<div class="col-sm-4"></div>
			<input type="submit" style="width: 150px;" value="ログイン"
				class="btn btn-dark">
		</div>
	</form>


</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userUpdate</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body style="margin-right: auto;margin-left: auto; font-family : serif">
     <div class="row" style="background-color:dimgrey">
    <div class="col-sm-8">　</div><h6 style="color: azure;padding-top: 8px">${getUser.name}　様</h6>
     　　<a href="LogoutServlet" style="color:crimson;text-decoration: underline;padding-top: 8px">ログアウト</a>
        </div>

<br>
    <h3 style="font-family:cursive;text-align: center">ユーザ情報更新</h3>
<br>
<c:if test="${alert!=null}">
<h6 style="color:crimson; text-align:center">${alert}</h6>
</c:if>
<br>
<div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-2"><h6 style="padding-top: 4px">ログインID</h6></div>
        <h6>${userList.loginId }</h6>

    </div>
    <br>
   <form action="UserUpdateServlet" method="post">
   <input type="hidden" name="userId" value="${userList.id }">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-2"><h6 style="padding-top: 4px">パスワード</h6></div>
        <input type="password" name="password1"style="width: 300px;height:30px" class="form-control">
    </div>
    <br>
 <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-2"><h6 style="padding-top: 4px">パスワード（確認）</h6></div>
        <input type="password"  name="password2" style="width: 300px;height:30px" class="form-control">
    </div>
    <br>
        <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-2"><h6 style="padding-top: 4px">ユーザ名</h6></div>
        <input type="text"  name="userName" value="${userList.name }" style="width: 300px;height:30px" class="form-control">
    </div>
    <br>
    <div class="row">
    <div class="col-sm-2"></div>
        <div class="col-sm-2"><h6 style="padding-top: 4px">生年月日</h6></div>
        <input type="date"  name="birthDate" value="${userList.birthDate }" style="width: 300px;height:30px" class="form-control">
    </div>
    <br>

    <div class="row">
    <div class="col-sm-5"></div>
    <input type="submit" value="更新" style="width: 100px" class="btn btn-secondary">
    </div>
    </form>

    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-sm-2"></div>
    <a href="UserListServlet" style="text-decoration: underline;color: darkblue">戻る</a>
    </div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userList</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body style="margin-right: auto;margin-left: auto; font-family : serif">
 <div class="row" style="background-color:dimgrey">
    <div class="col-sm-8">　</div><h6 style="color: azure;padding-top: 8px">${getUser.name}　様</h6><!--ここで空白を入れている!-->
     　　<a href="LogoutServlet" style="color:crimson;text-decoration: underline;padding-top: 8px">ログアウト</a>
        </div>

<br>
    <h3 style="font-family:cursive;text-align: center">ユーザ一覧</h3>
<br>
<br>

<br>
    <div class="row">
        <div class="col-sm-2"style="padding: 0px"></div>
        <div class="col-sm-2"><h6>ログインID</h6></div>
        <input type="text" style="width: 300px;height:30px" class="form-control">

    </div>
    <br>
        <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-2"><h6>ユーザ名</h6></div>
        <input type="text" style="width: 300px;height:30px" class="form-control">
    </div>
   <br>

    <div class="row">
    <div class="col-sm-2"></div>
        <div class="col-sm-2"><h6>生年月日</h6></div>
         <input type="text"  value="年/月/日" style="width: 120px;height:30px" class="form-control"> <h6>　〜　　</h6><input type="text"  value="年/月/日" style="width: 120px;height:30px" class="form-control">
    </div>

   <br>
    <div class="row">
        <div class="col-sm-9"></div>
<input type="submit" value="　　検索　　"class="btn btn-secondary">
    </div>
    <hr style="border-color:dimgrey">

    <table border="1" align="center">
        <tr style="background-color: darkgray">
            <th>ログインID</th><th>　ユーザ名　</th><th>　　生年月日　　</th><th>　</th>
        </tr>
        <c:forEach var="user" items="${userList}" step="1" >
        <tr>
            <td>${user.loginId}</td><td>${user.name }</td><td>${user.birthDate }　</td>
            <td>　

            <a href="UserReferenceServlet?id=${user.id}" type="button" style=width:60px;height:35px  class="btn btn-primary">
            詳細</a>
            <c:if test="${user.loginId}=='${getUser.loginId}'" >
            <a href="UserUpdateServlet?id=${user.id}" type="button" style=width:60px;height:35px class="btn btn-success">
            更新</a>
            <a href="UserDeleteServlet?id=${user.id}" type="button" style=width:60px;height:35px class="btn btn-danger">
            削除</a>
            </c:if>
            </td>
        </tr>
        </c:forEach>



    </table>



</body>
</html>
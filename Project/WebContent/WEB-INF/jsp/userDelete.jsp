<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userDelete</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body style="margin-right: auto;margin-left: auto; font-family : serif">
     <div class="row" style="background-color:dimgrey">
    <div class="col-sm-8">　</div><h6 style="color: azure;padding-top: 8px">${getUser.name}　様</h6>
     　　<a href="LogoutServlet" style="color:crimson;text-decoration: underline;padding-top: 8px">ログアウト</a>
        </div>

<br>
    <h3 style="font-family:cursive;text-align: center">ユーザ削除確認</h3>
<br>
<br>
 <div class="row">
    <div class="col-sm-4"> </div>
   <h6>ログインID:</h6><h6>${userList.loginId }</h6>
    </div>
    <div class="row">
        <div class="col-sm-4"></div>
<h6>を本当に削除してよろしいでしょうか。</h6>
        </div>
    <br>
    <br>
    <br>
    <div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-2">
        <a href="UserListServlet" type="button" style="width: 120px" class="btn btn-secondary">キャンセル</a>
        </div>
        <form action="UserDeleteServlet" method="post">
        <input type="submit" value="OK" style="width: 100px" class="btn btn-secondary">
        <input type="hidden" name="id" value=${userList.id }>
        </form>

    </div>


</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userReference</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body style="margin-right: auto;margin-left: auto; font-family : serif">
     <div class="row" style="background-color:dimgrey">
    <div class="col-sm-8">　</div><h6 style="color: azure;padding-top: 8px">${getUser.name}　様</h6>
     　　<a href="LogoutServlet" style="color:crimson;text-decoration: underline;padding-top: 8px">ログアウト</a>
        </div>

<br>
    <h3 style="font-family:cursive;text-align: center">ユーザ情報詳細参照</h3>
<br>
<br>
 <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3"><h6 style="font-family:monospace">ログインID</h6>
            </div>
    <h6>${userList.loginId }</h6>
</div>
    <br>
<div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3"><h6 style="font-family:monospace">ユーザ名</h6>
            </div>
    <h6>${userList.name}</h6>
</div>
    <br>
<div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3"><h6 style="font-family:monospace">生年月日</h6>
            </div>
    <h6>${userList.birthDate }</h6>
</div>
    <br>
 <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3"><h6 style="font-family:monospace">登録日時</h6>
            </div>
    <h6>${userList.createDate }</h6>
</div>
    <br>
 <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3"><h6 style="font-family:monospace">更新日時</h6>
            </div>
    <h6>${userList.updateDate }</h6>
</div>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-sm-2"></div>
    <a href="UserListServlet" style="text-decoration: underline;color: darkblue">戻る</a>
    </div>


</body>
</html>